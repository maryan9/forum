FROM centos:7
RUN yum -y install java

ADD target/blog.jar .

EXPOSE 8080

CMD ["java", "-jar", "blog.jar"]