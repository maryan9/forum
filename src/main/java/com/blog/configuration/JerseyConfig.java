package com.blog.configuration;

import com.blog.ws.CommentResource;
import com.blog.ws.ReportResource;
import com.blog.ws.TopicResource;
import com.blog.ws.UserResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

@Component
@ApplicationPath("/blog")
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        register(TopicResource.class);
        register(UserResource.class);
        register(ReportResource.class);
        register(CommentResource.class);
    }
}
