package com.blog.dao;

import com.blog.model.Comment;

public interface CommentDao extends GenericDao<Comment> {

}
