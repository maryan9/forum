package com.blog.dao;

import java.util.List;

public interface GenericDao<T> {

    List<T> getBatch(int from, int amount);

    List<T> getAll();

    void create(T t);

    T update(T t);

    T find(Long id);

    void remove(Long id);
}
