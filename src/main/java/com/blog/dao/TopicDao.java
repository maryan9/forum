package com.blog.dao;

import com.blog.model.Topic;
import com.blog.service.Category;

import java.util.List;

public interface TopicDao extends GenericDao<Topic> {
    List<Topic> getTopicByCategory(Category category);
}
