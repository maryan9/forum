package com.blog.dao;

import com.blog.model.User;

public interface UserDao extends GenericDao<User> {
    User findUserByName(String name);
}
