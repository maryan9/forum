package com.blog.dao.mapper;

import com.blog.model.CommentReport;
import com.blog.model.UserReport;

import java.util.List;

public interface ReportMapper {

    List<UserReport> getTopCommentators(int limit);

    List<UserReport> getTopLikers(int limit);

    List<CommentReport> getComments(String userName);

    List<CommentReport> getTopics(String userName);
}
