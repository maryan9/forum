package com.blog.dao.orm;

import org.springframework.stereotype.Repository;

import com.blog.dao.CommentDao;
import com.blog.model.Comment;

@Repository
public class CommentDaoImpl extends GenericDaoImpl<Comment> implements CommentDao {

	public CommentDaoImpl() {
		super(Comment.class);
	}
}
