package com.blog.dao.orm;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;

import org.springframework.stereotype.Repository;

import com.blog.dao.GenericDao;

@Repository
public abstract class GenericDaoImpl<T> implements GenericDao<T> {

    @PersistenceContext
    protected EntityManager entityManager;

    private Class<T> type;

    protected GenericDaoImpl(Class<T> type) {
        this.type = type;
    }

    @Override
    public List<T> getBatch(int from, int amount) {
        CriteriaQuery<T> query = entityManager.getCriteriaBuilder().createQuery(type);
        query.select(query.from(type));
        TypedQuery<T> selectQuery = entityManager.createQuery(query).setFirstResult(from).setMaxResults(amount);
        return selectQuery.getResultList();
    }

    @Override
    public List<T> getAll() {
        CriteriaQuery<T> query = entityManager.getCriteriaBuilder().createQuery(type);
        query.select(query.from(type));
        TypedQuery<T> selectQuery = entityManager.createQuery(query);
        return selectQuery.getResultList();
    }

    @Override
    public void create(T object) {
        entityManager.persist(object);
    }

    @Override
    public T find(Long id) {
        return entityManager.find(type, id);
    }

    @Override
    public void remove(Long id) {
        entityManager.remove(find(id));
    }


    @Override
    public T update(T object) {
        return entityManager.merge(object);
    }
}
