package com.blog.dao.orm;

import com.blog.service.Category;
import org.springframework.stereotype.Repository;

import com.blog.dao.TopicDao;
import com.blog.model.Topic;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class TopicDaoImpl extends GenericDaoImpl<Topic> implements TopicDao {

    public TopicDaoImpl() {
        super(Topic.class);
    }

    @Override
    public List<Topic> getTopicByCategory(Category category) {
        TypedQuery<Topic> tq = entityManager.createQuery("from Topic WHERE category=?", Topic.class);
        List<Topic> topics = tq.setParameter(1,category).getResultList();
        return topics;
    }
}
