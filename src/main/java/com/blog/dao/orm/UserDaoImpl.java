package com.blog.dao.orm;

import com.blog.dao.UserDao;
import com.blog.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;

@Repository
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {
    public UserDaoImpl() {
        super(User.class);
    }

    @Override
    public User findUserByName(String name) {
        TypedQuery<User> tq = entityManager.createQuery("from User WHERE userName=?", User.class);
        User result = tq.setParameter(1, name).getSingleResult();
        return result;
    }
}
