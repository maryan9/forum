package com.blog.model;

import com.blog.service.UserDetailsCustom;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class LoggedUser extends org.springframework.security.core.userdetails.User implements UserDetailsCustom {
    private Long id;

    public LoggedUser(Long id, String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
        this.id = id;
    }

    @Override
    public Long getUserId() {
        return id;
    }
}
