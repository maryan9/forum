package com.blog.model;

import com.blog.service.Category;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "TOPIC" )
public class Topic extends BaseEntity {

    public Topic() {

    }

    public Topic(String title, Date creationDate, User user, Category category) {
        this.title = title;
        this.creationDate = creationDate;
        this.user = user;
        this.category = category;
    }

    @Version
    private Long version;

    @Column(name = "TITLE")
    private String title;

    @ManyToOne( cascade = CascadeType.ALL)
    private User user;

    @OneToMany(fetch = FetchType.EAGER)
    @OrderColumn(name = "COMMENT_ORDER")
    @JoinColumn(name = "TOPIC_ID")
    private List<Comment> comments;


    @Column(name = "CONTENT")
    @Lob
    private String content;

    @Column(name = "CATEGORY")
    @Enumerated(EnumType.STRING)
    private Category category;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(unique = true, updatable = false, name = "TOPIC_ID")
    private Set<User> topicLikes;

    public boolean addLike(User user) {
        return topicLikes.add(user);
    }

    public Set<User> getTopicLikes() {
        return topicLikes;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Category getCategory() {
        return category;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public boolean addComment(Comment comment) {
        return comments.add(comment);
    }

    @Override
    public String toString() {
        return "TopicInfo [id= " + getId() + ", name= " + getTitle() + ", creationDate= " + getCreationDate() + "]";
    }
}
