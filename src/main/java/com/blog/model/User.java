package com.blog.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Version;

import java.util.Date;

@Entity
@Table(name = "USER_ACCOUNT")
public class User extends BaseEntity {

    public User() {
    }

    public User(Date creationDate, String userName, Date lastActivityDate) {
        this.creationDate = creationDate;
        this.userName = userName;
        this.lastActivityDate = lastActivityDate;
    }

    @Version
    private Long version;

    @Column(name = "USERNAME", unique = true)
    private String userName;

    @Column(name = "LAST_ACTIVITY_DATE")
    private Date lastActivityDate;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "ENABLED")
    private Boolean enabled;

    @Column(name = "AUTHORITY")
    private String authority;


    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public String getAuthority() {
        return authority;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getLastActivityDate() {
        return lastActivityDate;
    }

    public void setLastActivityDate(Date lastActivityDate) {
        this.lastActivityDate = lastActivityDate;
    }
}
