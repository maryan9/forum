package com.blog.model;

public class UserReport {
    private String userName;
    private Long amount;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getUserName() {
        return userName;
    }

    public Long getAmount() {
        return amount;
    }
}
