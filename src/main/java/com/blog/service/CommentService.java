package com.blog.service;

import com.blog.model.Comment;

import java.util.List;

public interface CommentService {
	
	void addComment(Long userId, Long topicId, Comment comment);

	void addSubComment(Long commentId, Long userId, Comment subComment);

	List<Comment> getAll(Long topicId);
}
