package com.blog.service;

import com.blog.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.blog.dao.CommentDao;
import com.blog.dao.TopicDao;
import com.blog.model.Comment;
import com.blog.model.Topic;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentDao commentDao;

    @Autowired
    private TopicDao topicDao;

    @Autowired
    private UserDao userDao;

    @Override
    @Transactional
    public void addComment(Long topicId, Long userId, Comment comment) {
        Topic topic = topicDao.find(topicId);
        comment.setUser(userDao.find(userId));
        topic.addComment(comment);
        commentDao.create(comment);
        topicDao.update(topic);
    }

    @Override
    @Transactional
    public void addSubComment(Long commentId, Long userId, Comment subComment) {
        Comment superComment = commentDao.find(commentId);
        subComment.setUser(userDao.find(userId));
        superComment.getComments().add(subComment);
        commentDao.create(subComment);
        commentDao.update(superComment);
    }

    @Override
    public List<Comment> getAll(Long topicId) {
        return null;
    }
}
