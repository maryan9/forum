package com.blog.service;

public enum ReportName {
    MOST_ACTIVE_COMMENTATORS,
    MOST_ACTIVE_LIKERS,
    ALL_COMMENTS,
    ALL_TOPICS
}
