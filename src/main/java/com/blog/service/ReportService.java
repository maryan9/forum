package com.blog.service;

import com.blog.model.CommentReport;
import com.blog.model.UserReport;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;


@Secured("ROLE_ADMIN")
public interface ReportService {
    List<UserReport> generateLimitReport(ReportName report, int limit) throws IOException;
    @Transactional
    List<CommentReport> generateUserReport(ReportName report, String name) throws IOException;
}
