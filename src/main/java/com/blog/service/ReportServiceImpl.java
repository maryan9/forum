package com.blog.service;

import com.blog.model.CommentReport;
import com.blog.model.UserReport;
import com.blog.dao.mapper.ReportMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.WebApplicationException;
import java.io.IOException;
import java.util.*;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    ReportMapper userMapper;

    @Override
    public List<UserReport> generateLimitReport(ReportName report, int limit) throws IOException {

        switch (report) {
            case MOST_ACTIVE_COMMENTATORS:
                return userMapper.getTopCommentators(limit);
            case MOST_ACTIVE_LIKERS:
                return userMapper.getTopLikers(limit);
            default:
                throw new WebApplicationException();
        }
    }

    @Override
    public List<CommentReport> generateUserReport(ReportName report, String name) throws IOException {
        switch (report) {
            case ALL_COMMENTS:
                return userMapper.getComments(name);
            case ALL_TOPICS:
                return userMapper.getTopics(name);
            default:
                throw new WebApplicationException();
        }
    }
}
