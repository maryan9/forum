package com.blog.service;

import java.util.List;

import com.blog.model.Topic;
import com.blog.model.TopicSummary;

import org.springframework.security.access.annotation.Secured;

public interface TopicService {

	List<TopicSummary> getSummaries();

	void generateTopics();

	List<Topic> getBatch(int from, int amount);

	@Secured("ROLE_USER")
	void create(Long userId, Topic topic);

	Topic find(Long id);

	void remove(Long id);

	int getLikes(Long topicId);

	void addLike(Long userId, Long topicId);

	List<Topic> getTopicsByCategory(Category category);

	int getTopicsAmount();

}
