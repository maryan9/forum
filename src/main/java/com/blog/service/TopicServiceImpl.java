package com.blog.service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.blog.dao.UserDao;
import com.blog.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.blog.dao.TopicDao;
import com.blog.model.Topic;
import com.blog.model.TopicSummary;

@Service
@Transactional
public class TopicServiceImpl implements TopicService {

    @Autowired
    private TopicDao topicDao;

    @Autowired
    UserDao userDao;

    @Override
    public void generateTopics() {
        for (int i = 1; i <= 5; i++) {
            topicDao.create(new Topic("title" + i, new Date(), userDao.find(1L), Category.JAVA));
        }
    }

    @Override
    public List<TopicSummary> getSummaries() {
        return topicDao.getAll().stream()
                .map(TopicSummary::new)
                .collect(Collectors.toList());
    }

    @Override
    public List<Topic> getBatch(int from, int amount) {
        return topicDao.getBatch(from, amount)
                .stream()
                .sorted((o1, o2) -> o2.getCreationDate()
                        .compareTo(o1.getCreationDate())).collect(Collectors.toList());
    }

    @Override
    public void create(Long userId, Topic topic) {
        topic.setUser(userDao.find(userId));
        topicDao.create(topic);
    }

    @Override
    public Topic find(Long id) {
        return topicDao.find(id);
    }

    @Override
    public void remove(Long id) {
        topicDao.remove(id);
    }

    @Override
    public int getLikes(Long topicId) {
        Topic topic = topicDao.find(topicId);
        return topic.getTopicLikes().size();
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public void addLike(Long userId, Long topicId) {
        Topic topic = topicDao.find(topicId);
        User user = userDao.find(userId);
        topic.addLike(user);
    }

    @Override
    public int getTopicsAmount() {
        return topicDao.getAll().size();
    }

    @Override
    public List<Topic> getTopicsByCategory(Category category) {
        return topicDao.getTopicByCategory(category);
    }
}
