package com.blog.service;

import com.blog.dao.UserDao;
import com.blog.model.LoggedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
@Service
public class UserDetailsServiceImpl implements UserDetailsService  {

    @Autowired
    UserDao userDao;

    @Override
    public UserDetailsCustom loadUserByUsername(String s) throws UsernameNotFoundException {
        com.blog.model.User currentUser = userDao.findUserByName(s);
        Set<GrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority(currentUser.getAuthority()));
        return new LoggedUser(currentUser.getId(), currentUser.getUserName(), currentUser.getPassword(),
                authorities);
    }
}
