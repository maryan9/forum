package com.blog.service;

import com.blog.model.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    void create(User user);

    User find(Long id);
}
