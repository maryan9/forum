package com.blog.service;

import com.blog.dao.UserDao;
import com.blog.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Override
    @Transactional
    public List<User> getAll() {
        return null;
    }

    @Autowired
    private UserDao userDao;

    @Override
    @Transactional
    public void create(User user) {
        userDao.create(user);
    }

    @Override
    public User find(Long id) {
        return userDao.find(id);
    }
}
