package com.blog.ws;

import com.blog.model.Comment;
import com.blog.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/comment")
@Produces(MediaType.APPLICATION_JSON)
public class CommentResource {

    @Autowired
    private CommentService commentService;

    @POST
    @Path("/{topicId}/{userId}")
    public void createComment(@PathParam("topicId") Long topicId, @PathParam("userId") Long userId, Comment comment) {
        commentService.addComment(topicId, userId, comment);
    }

    @POST
    @Path("/subComment/{commentId}/{userId}")
    public void addSubComment(@PathParam("commentId") Long commentId, @PathParam("userId") Long userId, Comment comment) {
        commentService.addSubComment(commentId, userId, comment);
    }
}
