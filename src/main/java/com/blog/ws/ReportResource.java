package com.blog.ws;

import com.blog.model.CommentReport;
import com.blog.model.LoggedUser;
import com.blog.model.UserReport;
import com.blog.service.ReportName;
import com.blog.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.List;

@Path("/report")
@Produces(MediaType.APPLICATION_JSON)
public class ReportResource {

    @Autowired
    private ReportService reportService;

    @GET
    @Path("commentators/{limit}")
    public List<UserReport> mostActiveCommentators(@PathParam("limit") Integer number, @QueryParam(value = "reportName") ReportName reportName) throws IOException {
        return reportService.generateLimitReport(reportName, number);
    }

    @GET
    @Path("comments/{userName}")
    public List<CommentReport> getAllComments(@PathParam("userName") String username, @QueryParam(value = "reportName") ReportName reportName) throws IOException {
        return reportService.generateUserReport(reportName, username);
    }

    @GET
    @Path("/userId")
    public Long getUserId() {
        LoggedUser loggedUser = (LoggedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return loggedUser.getUserId();
    }

    @GET
    @Path("/userStatus")
    public Object getRoles() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[0];
    }
}
