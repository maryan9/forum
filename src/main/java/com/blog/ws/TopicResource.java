package com.blog.ws;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.PathParam;


import com.blog.service.Category;
import org.springframework.beans.factory.annotation.Autowired;

import com.blog.model.Comment;
import com.blog.model.Topic;
import com.blog.model.TopicSummary;
import com.blog.service.TopicService;

@Path("/topic")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TopicResource {

    @Autowired
    private TopicService topicService;

    @GET
    @Path("/list")
    public List<Topic> getAllTopics(@QueryParam("from") int from,
                                    @QueryParam("amount") int amount) throws IOException {
        return topicService.getBatch(from, amount);
    }

    @GET
    @Path("/list/amount")
    public int getTopicsAmount() {
        //return (topicService.getTopicsAmount()/6);
        int topicsAmount = topicService.getTopicsAmount();
        if (topicsAmount % 2 == 0) {
            return topicsAmount / 2;
        }
        return topicsAmount / 2 + 1;
    }

    @GET
    @Path("/list/head")
    public List<TopicSummary> getSummaries() {
        return topicService.getSummaries();
    }

    @POST
    @Path("/")
    public String generateTopics(@QueryParam("action") String action) {
        if (!Objects.equals(action, "process")) {
            throw new WebApplicationException("Not Found", 404);
        }
        topicService.generateTopics();
        return "Topics have been generated";
    }

    @GET
    @Path("/{id}")
    public Topic getTopicById(@PathParam("id") Long id) {
        return topicService.find(id);
    }

    @POST
    @Path("/create/{userId}")
    public void createTopic(@PathParam("userId") Long userId, Topic topic) {
        topicService.create(userId, topic);
    }

   /* @PUT
    @Path("/{topicId}")
    public String update(@PathParam("topicId") Long topicId, TopicInfo requestTopic)
            throws IOException {
        if (!Objects.equals(requestTopic.getId(), topicId)) {
            throw new WebApplicationException("Mismatched ID", 400);
        }
        topicService.update(requestTopic);
        return "TopicInfo has been updated";
    }*/

    @GET
    @Path("/comments/{id}")
    public List<Comment> getTopicComments(@PathParam("id") Long id) {
        return topicService.find(id).getComments();
    }

    @POST
    @Path("/like/{topicId}/{userId}")
    public void addLike(@PathParam("topicId") Long topicId, @PathParam("userId") Long userId) {
        topicService.addLike(userId, topicId);
    }

    @GET
    @Path("/likes/{topicId}")
    public int getLikes(@PathParam("topicId") Long topicId) {
        return topicService.getLikes(topicId);
    }


    @GET
    public List<Topic> getTopicsByCategory(@QueryParam("category") Category category) {
        return topicService.getTopicsByCategory(category);
    }
}
