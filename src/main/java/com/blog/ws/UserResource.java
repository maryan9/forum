package com.blog.ws;

import com.blog.model.User;
import com.blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {

    @Autowired
    UserService userService;

    //TODO: validation
    @POST
    @Path("/create")
    public void create(User user) {
        userService.create(user);
    }

    @GET
    @Path("/{id}")
    public User getUser(@PathParam("id") Long userId) {
        return userService.find(userId);
    }
}
