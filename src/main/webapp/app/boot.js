"use strict";
var http_1 = require("@angular/http");
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var router_deprecated_1 = require("@angular/router-deprecated");
var main_1 = require("./main");
var common_1 = require("@angular/common");
platform_browser_dynamic_1.bootstrap(main_1.Main, [http_1.HTTP_PROVIDERS, router_deprecated_1.ROUTER_PROVIDERS,
    { provide: common_1.LocationStrategy, useClass: common_1.HashLocationStrategy }
]);
//# sourceMappingURL=boot.js.map