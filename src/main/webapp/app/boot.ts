import {HTTP_PROVIDERS} from "@angular/http";
import {bootstrap} from "@angular/platform-browser-dynamic";
import {ROUTER_PROVIDERS} from "@angular/router-deprecated";
import {Main} from "./main";
import {LocationStrategy, HashLocationStrategy} from "@angular/common";
import {CustomTable} from "./topicsTable/topicsTable";
import {TopicInfo} from "./topic/topicInfo";
bootstrap(Main, [HTTP_PROVIDERS, ROUTER_PROVIDERS,
    {provide: LocationStrategy, useClass: HashLocationStrategy}
]);


