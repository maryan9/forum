"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var router_deprecated_1 = require("@angular/router-deprecated");
var core_1 = require("@angular/core");
var topicsTable_1 = require("./topicsTable/topicsTable");
var lastTopics_1 = require("./topicsTable/lastTopics");
var newTopic_1 = require("./topic/newTopic");
var http_1 = require("@angular/http");
var report_1 = require("./reports/report");
var topicInfo_1 = require("./topic/topicInfo");
var topicsByCategory_1 = require("./topicsTable/topicsByCategory");
var registration_1 = require("./user/registration");
var userInfo_1 = require("./user/userInfo");
var Main = (function () {
    function Main(http) {
        this.http = http;
    }
    Main.prototype.ngOnInit = function () {
        this.getUserStatus('/blog/report/userStatus');
    };
    Main.prototype.getUserStatus = function (url) {
        var _this = this;
        this.http.get(url)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.status = data.authority;
        }, function (err) { return console.error(err); });
    };
    Main = __decorate([
        core_1.Component({
            selector: 'main',
            templateUrl: 'app/main.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES]
        }),
        router_deprecated_1.RouteConfig([
            { path: '/topics', name: 'Topics', component: topicsTable_1.CustomTable, useAsDefault: true },
            { path: '/lastTopics', name: 'LastTopics', component: lastTopics_1.LastTopics },
            { path: '/newTopic', name: 'NewTopic', component: newTopic_1.NewTopic },
            { path: '/admin', name: 'Report', component: report_1.Report },
            { path: '/topic', component: topicInfo_1.TopicInfo, name: 'TopicInfo' },
            { path: '/category', component: topicsByCategory_1.TopicsByCategory, name: 'TopicsByCategory' },
            { path: '/registration', name: 'Registration', component: registration_1.Registration },
            { path: '/user', component: userInfo_1.UserInfo, name: 'UserInfo' },
        ]), 
        __metadata('design:paramtypes', [http_1.Http])
    ], Main);
    return Main;
}());
exports.Main = Main;
//# sourceMappingURL=main.js.map