import {RouteConfig, ROUTER_DIRECTIVES} from "@angular/router-deprecated";
import {Component} from "@angular/core";
import {CustomTable} from "./topicsTable/topicsTable";
import {LastTopics} from "./topicsTable/lastTopics";
import {NewTopic} from "./topic/newTopic";
import {Http, Response} from "@angular/http";
import {Report} from "./reports/report";
import {TopicInfo} from "./topic/topicInfo";
import {TopicsByCategory} from "./topicsTable/topicsByCategory";
import {Registration} from "./user/registration";
import {UserInfo} from "./user/userInfo";
@Component({
    selector: 'main',
    templateUrl: 'app/main.html',
    directives: [ROUTER_DIRECTIVES]
})

@RouteConfig([
    {path: '/topics', name: 'Topics', component: CustomTable, useAsDefault: true},
    {path: '/lastTopics', name: 'LastTopics', component: LastTopics},
    {path: '/newTopic', name: 'NewTopic', component: NewTopic},
    {path: '/admin', name: 'Report', component: Report},
    {path: '/topic', component: TopicInfo, name: 'TopicInfo'},
    {path: '/category', component: TopicsByCategory, name: 'TopicsByCategory'},
    {path: '/registration', name: 'Registration', component: Registration},
    {path: '/user', component: UserInfo, name: 'UserInfo'},
])
export class Main {

    constructor(public http:Http) {}

    private status:string;

    ngOnInit() {
        this.getUserStatus('/blog/report/userStatus');
    }

    getUserStatus(url:string) {
        this.http.get(url)
            .map((res:Response) => res.json())
            .subscribe(
                data => {
                    this.status = data.authority
                },
                err => console.error(err));
    }
}