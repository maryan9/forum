/**
 * Created by mfai on 18.08.2016.
 */
import {Component} from "@angular/core";
import {Http} from "@angular/http";
@Component({
    templateUrl: 'app/reports/report.html'
})
export class Report {

    constructor(public http:Http) {
    }

    private likers:Array<any>;
    private commentators:Array<any>;
    private comments:Array<any>;
    private topics:Array<any>;


    getCommentators(url:string) {
        return this.http.get(url)
            .map(res => res.json())
            .subscribe(
                data => {
                    this.commentators = data;
                },
                err => console.error(err));
    }

    getLikers(url:string) {
        return this.http.get(url)
            .map(res => res.json())
            .subscribe(
                data => {
                    this.likers = data;
                },
                err => console.error(err));
    }

    getComments(url:string) {
        return this.http.get(url)
            .map(res => res.json())
            .subscribe(
                data => {
                    this.comments = data;
                },
                err => console.error(err));
    }

    getTopics(url:string) {
        return this.http.get(url)
            .map(res => res.json())
            .subscribe(
                data => {
                    this.topics = data;
                },
                err => console.error(err));
    }

}