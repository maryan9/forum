import {Component, Input} from "@angular/core";
/**
 * Created by mfai on 23.08.2016.
 */
@Component({
    template: '<a (click)="node.toggle())">{{ node.data.content }}</a>'
})
class MyTreeNodeTemplate {
}

