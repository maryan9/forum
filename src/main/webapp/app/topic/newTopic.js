"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
/**
 * Created by mfai on 15.08.2016.
 */
var NewTopic = (function () {
    function NewTopic(http) {
        this.http = http;
    }
    NewTopic.prototype.ngOnInit = function () {
        this.getUserId('/blog/report/userId');
    };
    NewTopic.prototype.sendTopic = function (url, value) {
        var _this = this;
        var json = {};
        json = value;
        json.creationDate = Date.now();
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        console.log(json);
        return this.http.post(url, json, {
            headers: headers
        })
            .map(function (res) { return res.json(); })
            .subscribe(function (data) { return _this.postData = data; }, function (error) { return console.error(error); });
    };
    NewTopic.prototype.getUserId = function (url) {
        var _this = this;
        return this.http.get(url)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.id = data;
        }, function (err) { return console.error(err); });
    };
    NewTopic.prototype.getImageId = function (url) {
        return this.constructor.hasOwnProperty.length;
    };
    NewTopic = __decorate([
        core_1.Component({
            templateUrl: 'app/topic/newTopicForm.html'
        }), 
        __metadata('design:paramtypes', [http_1.Http])
    ], NewTopic);
    return NewTopic;
}());
exports.NewTopic = NewTopic;
//# sourceMappingURL=newTopic.js.map