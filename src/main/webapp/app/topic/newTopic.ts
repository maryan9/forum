import {Component} from "@angular/core";
import {Http, Headers} from "@angular/http";
/**
 * Created by mfai on 15.08.2016.
 */
@Component({
    templateUrl: 'app/topic/newTopicForm.html'
})
export class NewTopic {

    private postData:string;
    private id:string;

    constructor(public http:Http) {}

    ngOnInit() {
        this.getUserId('/blog/report/userId');
    }

    sendTopic(url:string, value:any) {
        var json:any = {};
        json = value;
        json.creationDate = Date.now();
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        console.log(json);
        return this.http.post(url,
            json, {
                headers
            })
            .map(res => res.json())
            .subscribe(
                data => this.postData = data,
                error => console.error(error)
            );
    }

    getUserId(url:string) {
        return this.http.get(url)
            .map(res => res.json())
            .subscribe(
                data => {
                    this.id = data
                },
                err => console.error(err));
    }

    getImageId(url:string) {
        return this.constructor.hasOwnProperty.length;
    }
}
