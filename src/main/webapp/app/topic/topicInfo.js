"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_deprecated_1 = require("@angular/router-deprecated");
var http_1 = require("@angular/http");
var TopicInfo = (function () {
    function TopicInfo(http, params) {
        this.http = http;
        this.topicId = params.get("id");
        this.getUserId('/blog/report/userId');
        this.category = params.get("category");
        console.log(this.comments);
    }
    TopicInfo.prototype.ngOnInit = function () {
        this.getTopic();
        this.getComments();
        this.getLikes();
        this.getUserStatus('/blog/report/userStatus');
    };
    TopicInfo.prototype.getTopic = function () {
        var _this = this;
        return this.http.get('/blog/topic/' + this.topicId)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.title = data.title;
            _this.userName = data.user.userName;
            _this.content = data.content;
        }, function (err) { return console.error(err); });
    };
    TopicInfo.prototype.getUserId = function (url) {
        var _this = this;
        return this.http.get(url)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.userId = data;
        }, function (err) { return console.error(err); });
    };
    TopicInfo.prototype.getComments = function () {
        var _this = this;
        return this.http.get('/blog/topic/comments/' + this.topicId)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.comments = data;
        }, function (err) { return console.error(err); });
    };
    TopicInfo.prototype.getLikes = function () {
        var _this = this;
        return this.http.get('/blog/topic/likes/' + this.topicId)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.likes = data;
        }, function (err) { return console.error(err); });
    };
    TopicInfo.prototype.setLike = function () {
        var _this = this;
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/blog/topic/like/' + this.topicId + '/' + this.userId, {
            headers: headers
        }).map(function (res) { return res.json(); })
            .subscribe(function (data) { return _this.postData = data; }, function (error) { return console.error(error); });
    };
    TopicInfo.prototype.getUserStatus = function (url) {
        var _this = this;
        this.http.get(url)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.status = data.authority;
        }, function (err) { return console.error(err); });
    };
    TopicInfo.prototype.getCategory = function () {
        var _this = this;
        return this.http.get('/blog/topic?category=' + this.category)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.title = data.title;
            _this.userName = data.user.userName;
            _this.content = data.content;
        }, function (err) { return console.error(err); });
    };
    TopicInfo.prototype.addComment = function (value) {
        var _this = this;
        var json = {};
        json.content = value;
        json.creationDate = Date.now();
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        console.log(json);
        return this.http.post('/blog/comment/' + this.topicId + '/' + this.userId, json, {
            headers: headers
        })
            .map(function (res) { return res.json(); })
            .subscribe(function (data) { return _this.postData = data; }, function (error) { return console.error(error); });
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], TopicInfo.prototype, "comments", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], TopicInfo.prototype, "commentContent", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], TopicInfo.prototype, "topicId", void 0);
    TopicInfo = __decorate([
        core_1.Component({
            templateUrl: 'app/topic/topic.html'
        }), 
        __metadata('design:paramtypes', [http_1.Http, router_deprecated_1.RouteParams])
    ], TopicInfo);
    return TopicInfo;
}());
exports.TopicInfo = TopicInfo;
//# sourceMappingURL=topicInfo.js.map