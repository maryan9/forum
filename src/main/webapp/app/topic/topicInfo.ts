import {Component, Inject, Input} from "@angular/core";
import {RouteParams} from "@angular/router-deprecated";
import {Response, Http, Headers} from "@angular/http";

@Component({
    templateUrl: 'app/topic/topic.html'
})
export class TopicInfo {
    //topicId:number;
    private userId: number;
    title: string;
    userName: string;
    @Input() comments: Array<any>;
    postData: string;
    subComments: Array<any>;
    content: string;
    likes: number;
    private status: string;
    private category: string;

    @Input() commentContent:string;

    @Input() topicId: string;

    constructor(public http: Http, params: RouteParams) {
        this.topicId = params.get("id");
        this.getUserId('/blog/report/userId');
        this.category = params.get("category");
        console.log(this.comments);
    }

    ngOnInit() {
        this.getTopic();
        this.getComments();
        this.getLikes();
        this.getUserStatus('/blog/report/userStatus');
    }

    getTopic() {
        return this.http.get('/blog/topic/' + this.topicId)
            .map((res: Response) => res.json())
            .subscribe(
                data => {
                    this.title = data.title;
                    this.userName = data.user.userName;
                    this.content = data.content;
                },
                err => console.error(err));
    }

    getUserId(url: string) {
        return this.http.get(url)
            .map(res => res.json())
            .subscribe(
                data => {
                    this.userId = data
                },
                err => console.error(err));
    }

    getComments() {
        return this.http.get('/blog/topic/comments/' + this.topicId)
            .map((res: Response) => res.json())
            .subscribe(
                data => {
                    this.comments = data;
                },
                err => console.error(err));
    }

    getLikes() {
        return this.http.get('/blog/topic/likes/' + this.topicId)
            .map((res: Response) => res.json())
            .subscribe(
                data => {
                    this.likes = data;
                },
                err => console.error(err));
    }

    setLike() {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.http.post('/blog/topic/like/' + this.topicId + '/' + this.userId,
            {
                headers
            }).map(res => res.json())
            .subscribe(
                data => this.postData = data,
                error => console.error(error)
            );
    }

    getUserStatus(url: string) {
        this.http.get(url)
            .map((res: Response) => res.json())
            .subscribe(
                data => {
                    this.status = data.authority
                },
                err => console.error(err));
    }

    getCategory() {
        return this.http.get('/blog/topic?category=' + this.category)
            .map((res: Response) => res.json())
            .subscribe(
                data => {
                    this.title = data.title;
                    this.userName = data.user.userName;
                    this.content = data.content;
                },
                err => console.error(err));
    }

    addComment(value: any) {
        var json: any = {};
        json.content = value;
        json.creationDate = Date.now();
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        console.log(json);
        return this.http.post('/blog/comment/' + this.topicId + '/' + this.userId,
            json, {
                headers
            })
            .map(res => res.json())
            .subscribe(
                data => this.postData = data,
                error => console.error(error)
            );
    }
}