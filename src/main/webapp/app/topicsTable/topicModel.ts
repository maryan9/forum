export interface Topic {
    id:number,
    title:string,
    category:string,
    creationDate:string,
    user:Object
}

