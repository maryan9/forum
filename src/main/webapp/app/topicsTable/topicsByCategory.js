"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by mfai on 26.08.2016.
 */
var core_1 = require("@angular/core");
var router_deprecated_1 = require("@angular/router-deprecated");
var http_1 = require("@angular/http");
var TopicsByCategory = (function () {
    function TopicsByCategory(http, params) {
        this.http = http;
        this.category = params.get("category");
    }
    TopicsByCategory.prototype.ngOnInit = function () {
        this.getTopics('/blog/topic?category=' + this.category);
    };
    TopicsByCategory.prototype.getTopics = function (url) {
        var _this = this;
        return this.http.get(url)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.topics = data;
        }, function (err) { return console.error(err); });
    };
    TopicsByCategory = __decorate([
        core_1.Component({
            templateUrl: 'app/topicsTable/topicsTable.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES]
        }), 
        __metadata('design:paramtypes', [http_1.Http, router_deprecated_1.RouteParams])
    ], TopicsByCategory);
    return TopicsByCategory;
}());
exports.TopicsByCategory = TopicsByCategory;
//# sourceMappingURL=topicsByCategory.js.map