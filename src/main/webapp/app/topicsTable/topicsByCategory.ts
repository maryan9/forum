/**
 * Created by mfai on 26.08.2016.
 */
import {Component} from "@angular/core";
import {ROUTER_DIRECTIVES, RouteParams} from "@angular/router-deprecated";
import {Http, Response} from "@angular/http";
import {Topic} from "./topicModel";
@Component({
    templateUrl: 'app/topicsTable/topicsTable.html',
    directives: [ROUTER_DIRECTIVES]
})
export class TopicsByCategory {
    public topics:Array<Topic>;
    public category:string;


    constructor(public http:Http, params:RouteParams) {
        this.category = params.get("category");

    }

    ngOnInit() {
        this.getTopics('/blog/topic?category='+ this.category);
    }

    getTopics(url:string) {
        return this.http.get(url)
            .map((res:Response) => res.json())
            .subscribe(
                data => {
                    this.topics = data
                },
                err => console.error(err));
    }
}
