"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
require('rxjs/Rx');
var http_1 = require("@angular/http");
var router_deprecated_1 = require("@angular/router-deprecated");
//import {PaginationDirective} from "angular2-bootstrap-pagination/directives/pagination.directive";
var CustomTable = (function () {
    function CustomTable(http) {
        this.http = http;
    }
    CustomTable.prototype.ngOnInit = function () {
        this.getTopics('/blog/topic/list?from=0&amount=10');
        this.getTotalAmount('blog/topic/list/amount');
    };
    CustomTable.prototype.getTopics = function (url) {
        var _this = this;
        return this.http.get(url)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.topics = data;
        }, function (err) { return console.error(err); });
    };
    CustomTable.prototype.getTotalAmount = function (url) {
        var _this = this;
        return this.http.get(url)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.amount = data;
        }, function (err) { return console.error(err); });
    };
    CustomTable = __decorate([
        core_1.Component({
            templateUrl: 'app/topicsTable/topicsTable.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES]
        }),
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], CustomTable);
    return CustomTable;
}());
exports.CustomTable = CustomTable;
//# sourceMappingURL=topicsTable.js.map