import {Component, Injectable} from '@angular/core';
import 'rxjs/Rx';
import {Http, Response} from "@angular/http";
import {ROUTER_DIRECTIVES} from "@angular/router-deprecated";
import {Topic} from "./topicModel";
//import {PaginationDirective} from "angular2-bootstrap-pagination/directives/pagination.directive";

@Component({
    templateUrl: 'app/topicsTable/topicsTable.html',
    directives: [ROUTER_DIRECTIVES]
})
@Injectable()
export class CustomTable {
    private topics: Array<Topic>;
    private topic: any;
    private amount: number;

    constructor(public http: Http) {
    }

    ngOnInit() {
        this.getTopics('/blog/topic/list?from=0&amount=10');
        this.getTotalAmount('blog/topic/list/amount')
    }

    getTopics(url: string) {
        return this.http.get(url)
            .map((res: Response) => res.json())
            .subscribe(
                data => {
                    this.topics = data
                },
                err => console.error(err));
    }

    getTotalAmount(url: string) {
        return this.http.get(url)
            .map((res: Response) => res.json())
            .subscribe(
                data => {
                    this.amount = data
                },
                err => console.error(err));
    }
}
