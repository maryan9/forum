"use strict";
/**
 * Created by mfai on 05.09.2016.
 */
var LogIn = (function () {
    function LogIn(http) {
        this.http = http;
    }
    LogIn.prototype.ngOnInit = function () {
        this.logIn();
    };
    LogIn.prototype.logIn = function () {
        return this.http.get('/login')
            .map(function (res) { return res.json(); })
            .subscribe(function (err) { return console.error(err); });
    };
    return LogIn;
}());
exports.LogIn = LogIn;
//# sourceMappingURL=logIn.js.map