/**
 * Created by mfai on 26.08.2016.
 */
import {Component} from "@angular/core";
import {Http, Headers} from "@angular/http";
@Component({
    templateUrl: 'app/user/registerForm.html'
})

export class Registration {

    private postData: string;
    private enabled: boolean;
    private authority: string;

    constructor(public http: Http) {
    }

    createUser(url: string, value: any) {
        var json: any = {};
        json = value;
        json.creationDate = Date.now();
        json.enabled = true;
        json.authority = 'ROLE_USER';
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        console.log(json);
        return this.http.post(url,
            json, {
                headers
            })
            .map(res => res.json())
            .subscribe(
                data => this.postData = data,
                error => console.error(error)
            );
    }
}
