/**
 * Created by mfai on 29.08.2016.
 */
import {Component} from "@angular/core";
import {Http, Response} from "@angular/http";
import {RouteParams} from "@angular/router-deprecated";
@Component({
    templateUrl: 'app/user/userInfo.html'
})

export class UserInfo {
    private id: string;
    private userName: string;
    private creationDate: Date;
    private status:boolean;
    constructor(public http: Http, params: RouteParams) {
        this.id = params.get("id");
    }

    ngOnInit() {
        this.getUser();
    }

    getUser() {
        return this.http.get('/blog/user/' + this.id)
            .map((res: Response) => res.json())
            .subscribe(
                data => {
                    this.userName = data.userName;
                    this.creationDate = new Date(data.creationDate);
                    this.status = data.enabled;
                },
                err => console.error(err));
    }

}


