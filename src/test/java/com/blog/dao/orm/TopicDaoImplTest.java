/*
package com.blog.dao.orm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.metamodel.EntityType;

import com.blog.configuration.Config;
import com.blog.model.User;
import com.blog.service.Category;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.blog.dao.TopicDao;
import com.blog.model.Topic;

import static com.blog.test.ModelTestHelper.assertTopicEquals;;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@TestPropertySource(locations = "classpath:test-db.properties")
@Transactional
public class TopicDaoImplTest {

	private static final Date DATE_2015_10 = new Date(1445644800000L); // 2015-10-24T00:00:00

	@PersistenceContext
	private EntityManager entityManager;

	@Before
	public void setup() {

		expectedTopics = new ArrayList<>();
		for (long i = 0; i < 5; i++) {
			Topic topic = new Topic("topic"+i, DATE_2015_10, new User(), Category.C);

			topic.setId(i);
			expectedTopics.add(topic);
		}
		cleanUp();
	}

	private List<Topic> expectedTopics;

	@Autowired
	private TopicDao topicDao;

	@Test
	public void testGenerateTopics() {
		for (int i = 0; i < 5; i++) {
			Long topicId = topicDao.generateTopic().getId();

			assertNotNull(topicId);

			expectedTopics.get(i).setId(topicId);
		}

		assertEquals(5, topicDao.getAll().size());

		topicDao.getAll().sort((Topic o1, Topic o2) -> o1.getTitle().compareTo(o2.getTitle()));

		for (int i = 0; i < 5; i++) {
			assertTopicEquals(expectedTopics.get(i), topicDao.getAll().get(i));
		}
	}

	@Test
	@Transactional
	public void testUpdate() {
		Topic topic = topicDao.generateTopic(1);

		topic.setCreationDate(DATE_2015_10);
		topic.setTitle("NEW TITLE");

		topicDao.update(topic);

		Topic expected = new Topic("NEW TITLE", DATE_2015_10, "author");
		expected.setId(topic.getId());
		assertTopicEquals(expected, topicDao.getAll().get(0));
	}

	public void cleanUp() {
		for (EntityType<?> entity : entityManager.getMetamodel().getEntities()) {
			entityManager.createQuery("DELETE FROM " + entity.getName()).executeUpdate();
		}
		entityManager.flush();
		entityManager.clear();
	}
}
*/
