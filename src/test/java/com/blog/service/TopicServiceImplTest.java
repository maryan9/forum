
package com.blog.service;

import com.blog.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.blog.model.Topic;
import com.blog.model.TopicSummary;
import static com.blog.test.ModelTestHelper.assertTopicEquals;
import static com.blog.test.ModelTestHelper.assertTopicSummariesEqual;;;

@RunWith(MockitoJUnitRunner.class)
public class TopicServiceImplTest {

	private final static Date DATE_2015_09 = new Date(1443052800000L); //2015-09-24T00:00:00
	@Mock
	private TopicService topicService;

	private List<Topic> topics;
	private List<TopicSummary> topicsSummaries;

	@Before
	public void setup() {
		topics = new ArrayList<>();
		Topic topic1 = new Topic("Title", DATE_2015_09, new User(), Category.JAVA);
		topic1.setId(1L);
		topics.add(topic1);
		
		Topic topic2 = new Topic("Title1", DATE_2015_09, new User(), Category.JAVA);
		topic2.setId(2L);
		topics.add(topic2);

		topicsSummaries = new ArrayList<>();
		topicsSummaries.add(new TopicSummary(topic1));
		topicsSummaries.add(new TopicSummary(topic2));
		when(topicService.getBatch(0,topics.size())).thenReturn(topics);
		//when(topicService.getTopicById(2L)).thenReturn(new Topic("title",DATE_2015_09, "author"));
		when(topicService.getSummaries()).thenReturn(topicsSummaries);
	}

	@Test
	public void testGetAllTopics() {
		assertEquals(topics, topicService.getBatch(0,topics.size()));
	}

/*
	@Test
	public void testGetTopicById() {
		assertTopicEquals(new Topic("Title1", DATE_2015_09, new User(), Category.JAVA), topicService.getTopicById(2L));
	}
*/

	@Test
	public void testGetSummaries() {
		for(int i = 0; i < topicsSummaries.size(); i++) {
			assertTopicSummariesEqual(topicsSummaries.get(i), topicService.getSummaries().get(i));
		}
	}
}
