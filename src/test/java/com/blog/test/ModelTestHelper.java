package com.blog.test;

import static org.junit.Assert.assertEquals;

import com.blog.model.Topic;
import com.blog.model.TopicSummary;

public class ModelTestHelper {
	public static void assertTopicEquals(Topic expectedTopic, Topic actualTopic) {
		assertEquals(expectedTopic.getId(), actualTopic.getId());
		assertEquals(expectedTopic.getTitle(), actualTopic.getTitle());
		assertEquals(expectedTopic.getCreationDate(), actualTopic.getCreationDate());
	}

	public static void assertTopicSummariesEqual(TopicSummary expectedTopic, TopicSummary actualTopic) {
		assertEquals(expectedTopic.getId(), actualTopic.getId());
		assertEquals(expectedTopic.getTitle(), actualTopic.getTitle());
	}
}
